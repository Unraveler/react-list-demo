## Start the application
### Requirements
In order to run this application, be sure to have :
- [Docker](https://docs.docker.com/get-docker/)
- [Docker compose plugin](https://docs.docker.com/compose/install/)
- [Node and npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) (Node v16.16.0 and npm v8.11.0 used in this demo)

### Start
- Install the required packages by running `npm install`, on the root folder of this project;
- Once the packages are installed and while on the root of this project, run `docker compose up --build`;
- When `Compiled successfully!` message comes up, visit `http://localhost:3000` to access the application

## Application usage
This app shows a list of books provided by [OpenLibrary](https://openlibrary.org).

The user is greeted by the welcome screen by visiting `http://localhost:3000`. From the welcome screen, the user has the possibility to navigate to the available book list by clicking on `Books`.

After clicking on `Books`, the list is fetched from the `OpenLibrary`. This list consists on 50 books on the programming subject. Clickin on a `book title` on the list, reveals the `selected book details`.

The details can be edited by simply clicking on the button at the top that reads `Edit`. This will make the `book details` editable. The edit button now reads `Save`. Clicking on it, will save the chnages made. To discard the made edits, simply close the `book details` drawer by clicking on `X` at the top, or by choosing another book from the list.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run lint`
Checks the app for linting issues (warnings and errors). 

### `npm run typecheck`
Checks the app for type issues related

### `npm run check`
Runs `lint` and `typecheck`
