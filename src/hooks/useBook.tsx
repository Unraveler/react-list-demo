import {
  useContext,
  useEffect,
  useMemo,
  useState
} from 'react';
import { useParams } from 'react-router-dom';
import { message } from 'antd';
import { BookContext } from '../providers';
import { Book, EditBook, Subject, Work } from '../types';

export const useBook = () => {
  const { bookId } = useParams();
  const { books, setBooks } = useContext(BookContext);

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isEditing, setIsEditing] = useState<boolean>(false);

  useEffect(() => {
    if(books.length === 0) {
      getBooks();
    }
  }, [books]);

  useEffect(() => {
    setIsEditing(false);
  }, [bookId]);

  const bookDetails: Book = useMemo(() => {
    if (books.length > 0) {
      return books?.find((book: Book) => Number(book.olwid) === Number(bookId));
    }
    return undefined;
  }, [books, bookId]);

  /**
   * @description: Fetches the required book data from an API, formats it and sets the formatted data on the state
   */
  const getBooks: () => void  = async() => {
    try {
      setIsLoading(true);
      const response = await fetch('https://openlibrary.org/subjects/programming.json?details=true&limit=50');
      if (!response.ok) {
        return;
      }

      const data: Subject = await response.json();

      const formatedData: Book[] = data.works.map((work: Work) => {
        const splittedWorkKey = work.key.split('/');
        // Also remove the first two characters and the last character
        const workKey = String(splittedWorkKey[splittedWorkKey.length -1].slice(2, -1));

        return {
          olwid: workKey,
          title: work.title,
          coverMedium: `https://covers.openlibrary.org/b/olid/${work.cover_edition_key}-M.jpg`,
          coverSmall: `https://covers.openlibrary.org/b/olid/${work.cover_edition_key}-S.jpg`,
          authors: work.authors.map(author => author.name),
          isbn: work.availability && work.availability.isbn ? work.availability.isbn : 'ISBN not available',
          publishYear: work.first_publish_year
        };
      });
      setBooks(formatedData);
    } catch (error) {
      setIsLoading(false);
      message.error('An error occured while retrieving books', 5);
    } finally {
      setIsLoading(false);
      message.success('Books succefully retrieved', 5);
    }
  };

  /**
   * @description: Sets the new book details when saving edits
   */
  const onSaveBookDetails: (newValues: EditBook) => void = (newValues: EditBook) => {
    const updatedBook: Book | undefined = bookDetails;
    if (updatedBook) {
      updatedBook.title = newValues.title;
      updatedBook.authors = newValues.authors;
      updatedBook.isbn = newValues.isbn;
      updatedBook.publishYear = newValues.publishYear;

      books[books.indexOf(bookDetails)] = updatedBook;
    }
    setBooks(books);
  };

  const onActionButtonClick = (newValues) => {
    setIsEditing(!isEditing);

    if(isEditing) {
        onSaveBookDetails(newValues);
    }
  };

  return {
    books,
    bookDetails,
    isEditing,
    isLoading,
    setIsEditing,
    onSaveBookDetails,
    onActionButtonClick
  };
};
