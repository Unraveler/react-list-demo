import React from 'react';
import {createRoot} from 'react-dom/client';
import { App } from './components/App';
import 'antd/dist/reset.css';
import './index.css';

const appContainer = document.getElementById('root');
const appRoot = appContainer && createRoot(appContainer);

appRoot?.render(<App />);
