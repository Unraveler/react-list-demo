import React, {
  PropsWithChildren,
  createContext,
  FC,
  useState
} from 'react';
import { Book, BookContextType } from '../types';

export const BookContext = createContext<BookContextType>( {
  books: [],
  setBooks: () => []
});

export const BookProvider: FC<PropsWithChildren>= ({ children }) => {
  const [books, setBooks] = useState<Book[]>([]);

  return (
    <BookContext.Provider value={{ books, setBooks }}>
      {children}
    </BookContext.Provider>
  );
};
