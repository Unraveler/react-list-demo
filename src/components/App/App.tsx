import React, { FC } from 'react';
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider
} from 'react-router-dom';
import { Home } from '../Home';
import { Books } from '../Books';
import { BookProvider } from '../../providers/Provider';

export const App: FC = () =>  {

  const router = createBrowserRouter(
    createRoutesFromElements(
      <>
        <Route path='/' element={<Home />} />
        <Route path='/books' element={<Books />}/>
        <Route path='/books/:bookId' element={<Books />} />
      </>
    )
  );

  return (
    <BookProvider>
      <RouterProvider router={router} />
    </BookProvider>
  );
};
