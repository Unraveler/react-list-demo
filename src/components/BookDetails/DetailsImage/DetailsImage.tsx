import React, { FC } from 'react';
import { Image } from 'antd';
import { useBook } from '../../../hooks';

export const DetailsImage: FC<Record<string, never>> = () => {

  const { bookDetails } = useBook();

  return (
    <Image
      src={bookDetails.coverMedium}
      preview={false}
      fallback='/logo512.png'
    />
  );
};
