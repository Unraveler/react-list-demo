import React, { FC } from 'react';
import { Descriptions, Form, Input, Space } from 'antd';
import { EditBook } from '../../../types';
import { useBook } from '../../../hooks';
import { DetailsImage } from '../DetailsImage';
import { DropdownAuthorSelect } from '../DropDownSelectAuthor';

interface DetailsFormProps {
  isEditing: boolean;
  changedValues: EditBook;
  setChangedValues: (values: EditBook) => void;
}

export const DetailsForm: FC<DetailsFormProps> = ({
  isEditing,
  changedValues,
  setChangedValues
}) => {
  const { bookDetails } = useBook();

  return (
    <>
      <DetailsImage />
      <Form key={`${bookDetails.olwid}-form`}>
        <Form.Item
          label="Title"
          key={`${bookDetails.olwid}-form-title`}
        >
            {isEditing ? (
              <Input
                key={`${bookDetails.olwid}-form-title-input`}
                value={changedValues.title}
                onChange={(event) => setChangedValues({...changedValues, title: event.target.value})}
              />
            ) : (
              <Descriptions.Item>{bookDetails.title}</Descriptions.Item>
            )}
        </Form.Item>
        <Form.Item
          label="Authors"
          key={`${bookDetails.olwid}-form-authors`}
        >
          {isEditing ? (
            <DropdownAuthorSelect
              authorList={bookDetails.authors}
              showAddNewEntry={true}
              selectedAuthors={changedValues.authors}
              onSelectNewAuthor={(value) => setChangedValues({...changedValues, authors: value})}
            />
          ) : (
            bookDetails.authors.map((author: string, index: number) => (
              <div key={`${bookDetails.olwid}-form-authors-${author}-${index}`}>
                <Descriptions.Item> {author} </Descriptions.Item>
                <Space />
              </div>
            ))
          )}
        </Form.Item>
        <Form.Item
          label="ISBN"
          key={`${bookDetails.olwid}-form-isbn`}
        >
          {isEditing ? (
            <Input
              value={changedValues.isbn}
              onChange={(event) => setChangedValues({...changedValues, isbn: event.target.value})}
            />
          ) : (
            <Descriptions.Item>{bookDetails.isbn}</Descriptions.Item>
          )}
        </Form.Item>
        <Form.Item
          label="Publish Year"
          key={`${bookDetails.olwid}-form-publish_date`}
        >
          {isEditing ? (
            <Input
              value={changedValues.publishYear}
              onChange={(event) => setChangedValues({...changedValues, publishYear: String(event.target.value)})}
            />
          ) : (
            <Descriptions.Item>{bookDetails.publishYear}</Descriptions.Item>
          )}
        </Form.Item>
      </Form>
    </>
  );
};
