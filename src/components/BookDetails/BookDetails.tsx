import React, { FC, ReactNode } from 'react';
import { useNavigate } from 'react-router-dom';
import { CloseOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { useBook } from '../../hooks';
import './BookDetails.css';

interface BookDetailsProps {
  actionButtons: ReactNode[];
  details: ReactNode;
}

export const BookDetails: FC<BookDetailsProps> = ({
  actionButtons,
  details
}) => {
  const navigate = useNavigate();
  const { bookDetails } = useBook();

  return (
    <div className='book-details' >
      <div className='book-details__details-header'>
        <Button
          type='link'
          icon={<CloseOutlined />}
          onClick={() => navigate('/books')}
        />
        <h2 className='book-details__details-header-title'>{bookDetails.title}</h2>
        {actionButtons.map(actionButton => actionButton)}
      </div>
      <div className='book-details__details-body'>
        {details}
      </div>
    </div>
  );
};
