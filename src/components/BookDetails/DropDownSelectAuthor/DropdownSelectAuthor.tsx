import React, {
  FC,
  useRef,
  useState
} from 'react';
import {
  Button,
  Divider,
  Input,
  // NOTE: Getting the following lint error:
  // "InputRef not found in 'antd'"
  // This is actually imported from antd, didn't figure out why this error is shown.
  // Therefore I am ignoring it
  // eslint-disable-next-line
  InputRef,
  Select,
  Space
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';

interface DropdownAuthorSelectProps {
  authorList: string[];
  showAddNewEntry?: boolean;
  selectedAuthors: string[];
  onSelectNewAuthor: (value: string[]) => void;
}

export const DropdownAuthorSelect: FC<DropdownAuthorSelectProps> = ({
  authorList,
  showAddNewEntry = false,
  selectedAuthors,
  onSelectNewAuthor
}) => {
  const inputRef = useRef<InputRef>(null);
  const [authList, setAuthList] = useState<string[]>(authorList);

  return (
    <Select
      mode="multiple"
      allowClear
      placeholder="Select author"
      value={selectedAuthors}
      onChange={(value) => onSelectNewAuthor(value)}
      dropdownRender={menu => (
        <>
          {menu}
          {showAddNewEntry && (
            <>
              <Divider/>
              <Space>
                <Input
                  placeholder="Please enter author name"
                  ref={inputRef}
                />
                <Button
                  type="text"
                  icon={<PlusOutlined />}
                  onClick={(e) => {
                    e.preventDefault();
                    const newAuthor: string | undefined = inputRef.current?.input?.value;
                    if (newAuthor) {
                      setAuthList([...authList, newAuthor]);
                    }
                  }}
                >
                  Add author
                </Button>
              </Space>
          </>
          )}
        </>
      )}
      options={authList.map(author => ({
        label: author,
        value: author
      }))}
    />
  );
};
