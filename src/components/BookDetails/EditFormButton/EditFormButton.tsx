import React, { FC } from 'react';
import { EditOutlined } from '@ant-design/icons';
import { Button } from 'antd';

interface EditFormButtonProps {
  onClick: (e?: MouseEvent) => void;
  isEditing: boolean;
}

export const EditFormButton: FC<EditFormButtonProps> = ({
  onClick,
  isEditing
}) => (
  <Button
    icon={<EditOutlined />}
    onClick={() => onClick()}
  >
    {!isEditing ? 'Edit' : 'Save'}
  </Button>
);
