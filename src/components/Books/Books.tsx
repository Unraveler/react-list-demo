import React, {
  FC,
  useEffect,
  useState
} from 'react';
import { useBook } from '../../hooks';
import { BookList } from '../BookList';
import { BookDetails } from '../BookDetails';
import { EditFormButton } from '../BookDetails/EditFormButton';
import { EditBook } from '../../types';
import { DetailsForm } from '../BookDetails/DetailsForm';
import './Books.css';

const defaultValues: EditBook = {
  title:'',
  authors: [''],
  isbn: '',
  publishYear: ''
};

export const Books: FC<Record<string, never>> = () => {
  const {
    books,
    bookDetails,
    isEditing,
    isLoading,
    onActionButtonClick
  } = useBook();

  const [changedValues, setChangedValues] = useState(defaultValues);

  useEffect(() => {
    if(bookDetails) {
      setChangedValues({
        title: bookDetails.title,
        authors: bookDetails.authors,
        isbn: bookDetails.isbn,
        publishYear: String(bookDetails.publishYear)
      });
    }
  }, [bookDetails, setChangedValues]);

  return (
    <div className='main'>
      <BookList
        books={books}
        isLoading={isLoading}
      />
      {bookDetails && (
        <div className='main__book-information'>
          <BookDetails
            actionButtons={[
              (<EditFormButton
                key={`${bookDetails.olwid}-actionButton`}
                onClick={() => onActionButtonClick(changedValues)}
                isEditing={isEditing}
              />)
            ]}
            details={(
              <DetailsForm
                isEditing={isEditing}
                changedValues={changedValues}
                setChangedValues={setChangedValues}
              />
            )}
          />
        </div>
      )}
    </div>
  );
};
