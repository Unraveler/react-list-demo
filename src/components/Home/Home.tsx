import React, { FC } from 'react';
import { List } from 'antd';
import { Link } from 'react-router-dom';
import { HomeItem } from '../../types';

export const Home: FC<Record<string, never>> = () => {
  const HomeItems: HomeItem[] = [
    {
      title: 'Books',
      linkTo: 'books/',
      description: 'A list of books'
    }
  ];

  return (
    <>
      <h1> Welcome </h1>
      <List
        className='home-list'
        itemLayout="horizontal"
        dataSource={HomeItems}
        renderItem={(item: HomeItem, index: number) => {
          return (
            <List.Item key={index}>
              <List.Item.Meta
                title={
                  <Link to={item.linkTo}>
                    {item.title}
                  </Link>
                }
                description={item.description}
              />
            </List.Item>
          );
        }}
      />
    </>
  );
};
