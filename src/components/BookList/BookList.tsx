import React, { FC } from 'react';
import { Avatar, List } from 'antd';
import { Link } from 'react-router-dom';
import { Book } from '../../types';

interface BookListProps {
  books: Book[];
  isLoading: boolean;
}

export const BookList: FC<BookListProps> = ({
  isLoading,
  books
}) => {
  return (
    <List
      className='main__book-list'
      loading={isLoading}
      itemLayout="horizontal"
      dataSource={books}
      renderItem={(book: Book) => {
        return (
          <List.Item key={`${book.olwid}-item`}>
            <List.Item.Meta
              avatar={<Avatar src={book.coverSmall} />}
              title={
                <Link to={`/books/${book.olwid}`}>
                  {book.title}
                </Link>}
              description={book.isbn}
            />
          </List.Item>
        );
      }}
    />
  );
};
