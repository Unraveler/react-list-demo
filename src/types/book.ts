export interface Book {
  title: string;
  coverMedium: string;
  coverSmall: string;
  olwid: string;
  isbn: string;
  authors: string[];
  publishYear: number | string;
}

export interface EditBook {
  title: string;
  isbn: string;
  authors: string[];
  publishYear: number | string;
}

export interface Subject {
  key: string;
  name: string;
  subject_type: string;
  work_count: number;
  works: Work[];
}

interface Author {
  key: string;
  name: string;
}

interface Availability {
  status: string;
  available_to_browse: boolean;
  available_to_borrow: boolean;
  available_to_waitlist: boolean;
  is_printdisabled: boolean;
  is_readable: boolean;
  is_lendable: boolean;
  is_previewable: boolean;
  identifier: string;
  isbn: string;
  oclc?: unknown,
  openlibrary_work: string;
  openlibrary_edition: string;
  last_loan_date?: string;
  num_waitlist?: string;
  last_waitlist_date?: string,
  is_restricted: boolean;
  is_browseable: boolean;
  __src__: string;
}

export interface Work {
  authors: Author[];
  availability:Availability;
  cover_edition_key: string;
  cover_id: number;
  edition_count: number;
  first_publish_year: number;
  has_full_text: boolean;
  ia: string;
  ia_collection: string[];
  key: string;
  lending_edition: string;
  lending_identifier: string;
  lendinglibrary: boolean;
  printdisabled: boolean;
  public_scan: boolean;
  subject: string[];
  title: string;
}
