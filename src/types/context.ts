import { Dispatch } from 'react';
import { Book } from './book';

export interface BookContextType {
  books: Book[];
  setBooks: Dispatch<Book[]>;
}
