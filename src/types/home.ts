export interface HomeItem {
  title: string;
  linkTo: string;
  description: string;
}
